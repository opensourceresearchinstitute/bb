#ifndef LOGCHANA_H
#define LOGCHANA_H

#include <chana.h>
#include <iostream>

struct LogChanA: ChanA {
  LogChanA(std::ostream& os_);
  virtual int send(const std::vector<std::string>& data) override;
  
private:
  std::ostream& os;
};

#endif
