#ifndef SUB1_H
#define SUB1_H

#include <string>

/*
 This is the interface used by a pubsub system when it wants to push a message
 into the aggregator.
 The onNext() is where the main work is done for an incoming message.
 */
struct Sub1 {
  virtual int onInit() = 0; //called when agg first subscribes
  virtual int onNext(const std::string& x) = 0; //this is where work gets done
  virtual int onError() = 0;
  virtual int onDone() = 0; //called after agg unsubscribes, to clean up
};

#endif
