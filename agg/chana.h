#ifndef CHANA_H
#define CHANA_H

#include <string>
#include <vector>

struct ChanA {
  virtual int send(const std::vector<std::string>& data) = 0;
};

#endif
