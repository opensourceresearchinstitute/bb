#include <agg.h>
#include <cassert>
#include <chana.h>
using namespace std;

Agg::Agg(): subs(0) {
}

// PubA
int Agg::onSub() {
  const lock_guard<mutex> lock(zone1mutex);
  ++subs;
  return 0;
}

int Agg::onUnsub() {
  const lock_guard<mutex> lock(zone1mutex);
  assert(subs);
  --subs;
  return 0;
}

int Agg::send(ChanA* ch) {
  const lock_guard<mutex> lock(zone2mutex);
  ch->send(pending);
  pending.clear();
  return 0;
}
  
// Sub1
int Agg::onInit() {
  return 0;
}

int Agg::onNext(const std::string& x) {
  {
    const lock_guard<mutex> lock(zone3mutex);
    if (usage_.size() < x.size()+1)
      usage_.resize(x.size()+1, 0);
    ++usage_[x.size()];
  }
  
  const lock_guard<mutex> lock(zone1mutex);
  if (subs) {
    const lock_guard<mutex> lock(zone2mutex);
    // TODO mark dirty / schedule send call
    pending.push_back(x);
  }
  return 0;
}

int Agg::onError() {
  return 0;
}

int Agg::onDone() {
  return 0;
}

vector<size_t> Agg::usage() const {
  const lock_guard<mutex> lock(zone3mutex);
  return usage_;
}
