#include <algorithm>
#include <iterator>
#include <logchana.h>
using namespace std;

LogChanA::LogChanA(ostream& os_): os(os_) {
}

int LogChanA::send(const vector<string>& data) {
  copy(data.begin(), data.end(), ostream_iterator<std::string>(os, " "));
  os << '\n';
  return 0;
}
