#include <algorithm>
#include <chrono>
#include <mocksource1.h>

using namespace std;

MockSource1::MockSource1(initializer_list<string> data_):
  data(data_), t(&MockSource1::run, this)
{
}

void MockSource1::addSub(Sub1* sub) {
  const lock_guard<mutex> lock(mutex_);
  subs.push_back(sub);
  sub->onInit();
}

void MockSource1::removeSub(Sub1* sub) {
  const lock_guard<mutex> lock(mutex_);
  subs.erase(remove(subs.begin(), subs.end(), sub), subs.end());
}

void MockSource1::run() {
  using namespace chrono_literals;
  size_t i = 0;
  while (!stop_) {
    this_thread::sleep_for(50ms);
    const lock_guard<mutex> lock(mutex_);
    const std::string& x(data[i]);
    for_each(subs.begin(), subs.end(), [&](Sub1* s) { s->onNext(x); });
    i = (i+1) % data.size();
  }
}

void MockSource1::stop() {
  stop_ = true;
  t.join();
  std::vector<Sub1*> s;
  {
    //Nate: 
    const lock_guard<mutex> lock(mutex_);
    swap(s, subs);
  }
  for_each(s.begin(), s.end(), [](Sub1* x){ x->onDone(); });
}
