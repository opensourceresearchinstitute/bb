#ifndef PUBA_H
#define PUBA_H

struct ChanA;

/*
 Interface to a pubsub system, for when want to push out to the pubsub system.
 */
struct PubA {
  virtual int onSub() = 0; //called when get a new recipient of pushes
  virtual int onUnsub() = 0; //called when a recipient of pushes disappears
  virtual int send(ChanA* ch) = 0; //called from inside agg, to do a push
};

#endif
