#ifndef MOCKSOURCE1_H
#define MOCKSOURCE1_H

#include <atomic>
#include <initializer_list>
#include <mutex>
#include <string>
#include <thread>
#include <vector>
#include "sub1.h"

struct MockSource1 {
  MockSource1(std::initializer_list<std::string> data_);
  MockSource1(MockSource1&&) = default;
  MockSource1& operator=(MockSource1&&) = default;
  MockSource1(const MockSource1&) = delete;
  MockSource1& operator=(const MockSource1&) = delete;

  void addSub(Sub1* sub);
  void removeSub(Sub1* sub);
  void stop();
  
private:
  void run();

  std::vector<std::string> data;
  std::mutex mutex_;
  std::atomic<bool> stop_;
  std::vector<Sub1*> subs;
  std::thread t;
};

#endif
