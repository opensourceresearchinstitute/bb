#include <agg.h>
#include <atomic>
#include <chrono>
#include <cstdlib>
#include <iostream>
#include <logchana.h>
#include <mocksource1.h>
#include <puba.h>
#include <signal.h>
#include <thread>
using namespace std;

namespace {
  atomic<bool> stop;
  void handler(int sig) {
    stop = true;
  }
}

int main() {
  using namespace chrono_literals;
  
  Agg agg;
  LogChanA ch(cout);
  //  MockSource1 ms{"the","quick","brown","fox"}; //a thing on other side of a 
  //     //pub-sub system that is pushing into the agg object
  MockSource1 ms{
    "Four", "score", "and", "seven", "years", "ago", "our", "fathers",
    "brought", "forth", "on", "this", "continent", "a", "new", "nation",
    "conceived", "in", "Liberty", "and", "dedicated", "to", "the",
    "proposition", "that", "all", "men", "are", "created", "equal"
  };
  ms.addSub(&agg); //register the agg object as a subscriber to the source
     //normally this would be more complex, such as stating a topic or a filter
     // on message types.
     //this "addSub" call causes the source to start pushing into the agg object
     // on the source's own thread.
  agg.onSub(); //tell the agg object that it has just been registered as a
     // subscriber to a new source.

  signal(SIGINT, handler);
  
  //This adds a second thread.  It doesn't seem to really correspond to an
  // actual situation, but it does introduce two external threads acting on
  // the same agg object.
  //It kind of fakes being a pubsub system that periodically pulls from the
  // aggregator.  The pulls happen on the main thread, so agg's send() function
  // runs in the main thread.  Normally, send() would run either in the thread
  // of a pubsub system that is pushing to the agg object.
  // Or else it would run in a thread from the target pubsub system being pushed
  // to.  Either as a callback from that pubsub system (IE, the push-to-agg
  // thread registered with the agg-push-to system, which then called back),
  // or else in a thread that is polling the agg object to check for pending
  // pushes.
  while (!stop) {
    for (int i=0; !stop && i<10; ++i) {
      this_thread::sleep_for(250ms);
      agg.send(&ch);  //
    }
    auto u = agg.usage();
    for (int i=0; i<u.size(); ++i)
      cout << i << '\t' << string(u[i], '*') << '\n';
  }
  
  cout << "Shutting down...\n";
  ms.stop();
  return EXIT_SUCCESS;
}
