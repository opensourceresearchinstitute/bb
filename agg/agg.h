#ifndef AGG_H
#define AGG_H

#include <mutex>
#include <puba.h>
#include <string>
#include <sub1.h>
#include <vector>

/*
 The aggregator class.  has an interface for pushing to a pubsub system named
  PubA, and for receiving from a pubsub system named Sub1
 */
struct Agg: PubA, Sub1 {
  Agg();
  Agg(Agg&&) = default;
  Agg& operator=(Agg&&) = default;
  Agg(const Agg&) = delete;
  Agg& operator=(const Agg&) = delete;
  
  // PubA -- interface for pushing to pubsub system called PubA
  virtual int onSub() override;
  virtual int onUnsub() override;
  virtual int send(ChanA* ch) override;
  
  // Sub1 -- interface for receiving from pubsub system, named Sub1
  virtual int onInit() override;
  virtual int onNext(const std::string& x) override; //main work here.  Runs on
     //thread that exists inside the pubsub system that is pushing to agg object
  virtual int onError() override;
  virtual int onDone() override;

  std::vector<std::size_t> usage() const; // count of recv'd messages by size

private:
  // zone 1
  mutable std::mutex zone1mutex;
  std::size_t subs;                 // count of active subs

  // zone 2
  mutable std::mutex zone2mutex;
  std::vector<std::string> pending; // data to send

  // zone 3
  mutable std::mutex zone3mutex;
  std::vector<std::size_t> usage_;  // count of recv'd messages by size
};

#endif
