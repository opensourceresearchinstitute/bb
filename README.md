# README #

as of June 2016, this is a self-contained aggregator object, with its
own makefile.  Soon, this will morph into a skeleton that is connected
to synchronization behavior that is implemented via the proto-runtime
system.

At this point, the value is to re-create real world conditions that
are experienced by programmers who interact with multiple
publish-subscribe systems.  Their main work is to receive information,
via subscriptions, to perform computation on that info, and then to
push the results.

The issue they encounter is that each pub-sub system has its own
threads, which call-back into the aggregator object and modify
internal aggregator state.  Some of the modified state is specific to
the pub-sub system performing the callback, but some of the modified
state is shared.  It can be accessed by threads from different pub-sub
systems.

So, as of June, the code in this repository reproduces these effects.
It has an aggregator object, that has internal state that is modified
by a thread belonging to an external pub-sub that pushed data into the
aggregator.  That state is periodically read by a thread belonging to
a different external pub-sub that pulls data out.  If the timing is
bad, the writer and reader could be traversing at the same time and
the reader gets inconsistent results.

===================================

To compile, in top directory, type:

```
$ make
```

### How do I get set up? ###

  * Install VirtualBox
  * Create a VM running Linux Mint
    * For version, select Ubuntu (64-bit)
    * Suggested resource allocations: 8GB RAM and 32GB disk
    * Settings > General > Advanced > Shared Clipboard: Bidirectional
  * On the VM, install clang, git, and libc++-dev:

```
$ wget -O - http://llvm.org/apt/llvm-snapshot.gpg.key | sudo apt-key add -
$ sudo apt-add-repository 'deb http://llvm.org/apt/trusty/ llvm-toolchain-trusty-3.8 main'
$ sudo apt-get update
$ sudo apt-get install clang-3.8 lldb-3.8 libc++-dev git emacs24
$ sudo ln -s /usr/bin/clang++-3.8 /usr/bin/clang++
```

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### To whom do I talk? ###

* Repo owner or admin
* Other community or team contact
